import React, { Component, Fragment } from 'react';

import NewPost from '../NewPost'
import ListPosts from '../ListPosts'
import  articles from '../articles'

export default class Home extends Component {
  state = {
    active: false
  }
  render() {
    const { active } = this.state
    return (
      <Fragment>
        <div className="container">
          <button
            className="btn btn-primary mb-4"
            onClick={() => this.setState({active: !active})}
            > Nova Notícia 
          </button>
          <NewPost
            className="mb-4"
            isActive={this.state.active}
          />
          {/* Listagem de posts */}
          <ListPosts />
        </div>
      </Fragment>
    )
  }
}
