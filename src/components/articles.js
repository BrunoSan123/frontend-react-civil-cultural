import React, { Component, Fragment } from 'react';
import { Query } from 'react-apollo'
import { Mutation } from 'react-apollo'

import { GET_ARTICLES } from '../querys'
import { CREATE_ARTICLE } from '../mutations'

import { success, fail } from '../utils/toast_msg'

export default class articles extends Component {
  state ={
        id,
        title: '',
        abstract: '',
        body: '',
        references: ''
  }

}

render() {
    const { isActive } = this.props
    const { id,title,abstract,body,references } = this.state
    if(isActive){
        return(
           
           <Fragment>
<div className="box-create mb-4">
            <input 
              type="text"
              className="title-news" 
              placeholder="Título do artigo aqui." 
              onChange={(e) => {this.setState({ title: e.target.value })}}
              defaultValue={""} 
            />
            <div 
              className="content-editable"
              contentEditable="true"
              onInput={(e) => {this.setState({ body: e.target.innerHTML })}}
              data-placeholder="Conteúdo do artigo aqui."
              ></div>

<Query query={GET_ARTICLES}>
              {({ loading, error, data }) => {
                if (loading) return 'Loading.....'
                if (error) return `Error! ${error.message}`

                
              }}</Query>
              <Mutation
                mutation={CREATE_ARTICLE}
                variables={{ id,title,abstract,body,references }}
                onCompleted={() => success('Artigo criad0 com sucesso!', 1000)}
                onError={(error) => fail(error.message, 1000)}
                ></Mutation>
                </div>
           </Fragment> 
        )

        
    } else{
     return <Fragment></Fragment>
    }
}
