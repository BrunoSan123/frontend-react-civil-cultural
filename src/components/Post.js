import React from 'react'
import image from '../img/newspaper.jpeg'

export default function Posts(props) {
  const { data } = props
  return (
    <React.Fragment>
      <div className="row mb-4">
        <div className="col">
          <div className="news">
            <div className="title">{data.title}</div>
            <div className="small">
              <span className="date">19/09/2019</span>
              <span> | 
                por&nbsp;
                <a href="#!" className="author">
                  Leonardo Lima
                </a>
              </span>
            </div>
          </div>
          <div className="news-prev">
            <div className="news-body">
              {data.body}
            </div>
            {/* <img src={image} className="cover" height="150" /> */}
          </div>
      
          <button className="btn btn-primary"><a href="!#">Ver mais</a></button>
          
        </div>
      </div>
    </React.Fragment>
  )
}