import React, { Component, Fragment } from 'react'

import { Query } from 'react-apollo'
import { Mutation } from 'react-apollo'

import { GET_PORTALS } from '../querys'
import { CREATE_NEWS } from '../mutations'

import { success, fail } from '../utils/toast_msg'

export default class NewPost extends Component {
  state = {
    title: '',
    body: '',
    portal: ''
  }


  render() {
    const { isActive } = this.props
    const { title, body, portal } = this.state
    if (isActive) {
      return (
        <Fragment>
          <div className="box-create mb-4">
            <input 
              type="text"
              className="title-news" 
              placeholder="Título da notícia aqui." 
              onChange={(e) => {this.setState({ title: e.target.value })}}
              defaultValue={""} 
            />
            <div 
              className="content-editable"
              contentEditable="true"
              onInput={(e) => {this.setState({ body: e.target.innerHTML })}}
              data-placeholder="Conteúdo da notícia aqui."
              ></div>
            <Query query={GET_PORTALS}>
              {({ loading, error, data }) => {
                if (loading) return 'Loading portals...'
                if (error) return `Error! ${error.message}`

                return (
                  <select name="portal" onChange={(e) => {this.setState({ portal: e.target.value })}}>
                    <option selected> ---- Escolha um portal ---- </option>
                    {data.portals.edges.map((docs) => {
                      return (
                        <option 
                          key={docs.node.id} 
                          value={docs.node.id}
                          >{docs.node.name}</option>
                      )
                    }
                    )}
                  </select>
                )
              }}
            </Query>
            <div className="actions">
              <Mutation
                mutation={CREATE_NEWS}
                variables={{ title, body, portal }}
                onCompleted={() => success('Notícia criada com sucesso!', 1000)}
                onError={(error) => fail(error.message, 1000)}
                >
                  {(mutation) => (
                    <button 
                      className="btn btn-primary"
                      onClick={mutation}
                      disabled={!title || !body || !portal}
                      >Publicar</button>
                  )}
              </Mutation>
            </div>
          </div>
        </Fragment>
      )
    } else {
      return <Fragment></Fragment>
    }
  }
}