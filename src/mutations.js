import gql from 'graphql-tag';

export const CREATE_NEWS = gql`
  mutation createNews($title: String!, $body: String!, $portal: ID!) {
    createNews(input: {
      title: $title
      body: $body
      portal: $portal
    }) {
      news {
        id
        title
        body
      }
    }
  }
`;
export const CREATE_ARTICLE = gql`

mutation createArticle($title: string!, $abstract: String!, $body: String!, $references: ID!, $keywords: Strings!){
  createArticle(input:{
    title: $title
    abstract: $abstract
    body: $body
    references: $references
    keywords: $keywords
  }) {
      article{
        id
        title
        abstract
        body
        references
        author{
          id
          username
        }
        publicationDate
        keywords
        votes
        similarArticles{
          id
          articleId
          links
          votes
        }
      }

  }
}


`;

export const LOGIN_MUTATION = gql`
  mutation login($username: String!, $password: String!) {
    logIn(username: $username, password: $password) {
      token
    }
  }
`;

export const REGISTER_MUTATION = gql`
  mutation register($username: String!, $email: String!, $password: String!, $birthdate: Date!) {
    signUp(input:{
      username: $username,
      password: $password,
      email: $email,
      birthDate: $birthdate,
      clientMutationId: $username
    }) {
      clientMutationId
    }
  }
`;

export const LOGOUT = gql`
  mutation logout {
    logOut(input:{}) {
      response
    }
  }
`;