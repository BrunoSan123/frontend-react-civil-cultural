import gql from 'graphql-tag';

export const GET_NEWS = gql`
  query {
    news {
      edges {
        node {
          id
          title
          body
        }
      }
    }
  }
`;

export const GET_PORTALS = gql`
  query {
    portals {
      edges {
        node {
          id
          name
        }
      }
    }
  }
`;

export const GET_ARTICLES = gql `
query{
  articles{
    edges{
      node{
        id
        title
        abstract
        keywords
        body
        references
        publicationDate
        author{
          id
          username
        }
        similarArticles{
          id
          articleid
          links
          votes
        }
        votes
      }
    }
  }
}
`